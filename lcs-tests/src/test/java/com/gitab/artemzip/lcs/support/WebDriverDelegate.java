package com.gitab.artemzip.lcs.support;

import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.experimental.Delegate;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.stereotype.Component;

import static java.util.concurrent.TimeUnit.SECONDS;

@Component
public class WebDriverDelegate implements WebDriver, AfterEachCallback {

    static {
        if(!System.getProperties().containsKey("webdriver.chrome.driver")) {
            WebDriverManager.chromedriver().setup();
        }
    }

    private ChromeDriver delegate;

    @Delegate
    private WebDriver getDelegate(){
        if(delegate == null) {
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--start-maximized", "--window-size=1920,1080");
            options.addArguments("--headless", "--no-sandbox", "--disable-dev-shm-usage");

            delegate = new ChromeDriver(options);
            delegate.manage().timeouts().pageLoadTimeout(120, SECONDS);
            delegate.manage().timeouts().implicitlyWait(120, SECONDS);
            delegate.manage().timeouts().setScriptTimeout(120, SECONDS);
        }
        return delegate;
    }


    @Override
    public void afterEach(ExtensionContext extensionContext) throws Exception {
        if(delegate != null) {
            delegate.quit();
            delegate = null;
        }
    }
}
