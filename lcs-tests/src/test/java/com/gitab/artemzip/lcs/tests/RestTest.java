package com.gitab.artemzip.lcs.tests;

import com.gitab.artemzip.lcs.AbstractIT;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.SneakyThrows;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;

import static java.net.URI.create;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.http.HttpStatus.OK;


class RestTest extends AbstractIT {

    @Test
    @SneakyThrows
    void simpleDataTest() {
        ResponseEntity<String> entity = post("INPUT1", "INPUT2", "INPUT3");
        assertEquals(OK, entity.getStatusCode());
        assertEquals("INPUT", new JSONObject(entity.getBody()).getString("result"));
    }

    @Test
    @SneakyThrows
    void invalidDataTest() {
        assertThrows(HttpClientErrorException.UnprocessableEntity.class, () -> post("INPUT1", "", "INPUT3"));
        assertThrows(HttpClientErrorException.UnprocessableEntity.class, () -> post(null, "INPUT2", "INPUT3"));
    }

    private RequestEntity<Input> createTestEntity (String input1, String input2, String input3){
        return RequestEntity.post(create(CONTAINER.getUrl() + "lcs/solve"))
                            .body(new Input(input1, input2, input3));
    }

    private ResponseEntity<String> post(String input1, String input2, String input3) {
        return REST_CLIENT.exchange(createTestEntity(input1, input2, input3), String.class);
    }

    @Data
    @AllArgsConstructor
    class Input {
        private String input1;
        private String input2;
        private String input3;
    }
}
