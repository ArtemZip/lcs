package com.gitab.artemzip.lcs.tests;

import com.gitab.artemzip.lcs.AbstractIT;
import com.gitab.artemzip.lcs.page.LcsPage;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.TimeoutException;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class UiTest extends AbstractIT {

    @Autowired
    private LcsPage page;

    @Test
    void simpleDataTest() {
        page.open();
        assertTrue(page.isOpen());

        page.solve("INPUT1", "INPUT2", "INPUT3");
        assertEquals("INPUT", page.getResult());
    }

    @Test
    void invalidDataTest() {
        page.open();
        assertTrue(page.isOpen());

        page.solve("INPUT1", "", "INPUT3");
        assertThrows(TimeoutException.class, () -> page.getResult());
    }
}
