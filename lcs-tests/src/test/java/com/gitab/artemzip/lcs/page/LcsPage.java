package com.gitab.artemzip.lcs.page;

import com.gitab.artemzip.lcs.support.LcsContainer;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

import static org.openqa.selenium.support.ui.ExpectedConditions.attributeToBeNotEmpty;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

@Component
public class LcsPage {

    @Autowired
    private WebDriver driver;
    private WebDriverWait wait;

    @FindBy(id = "input1")
    private WebElement input1;

    @FindBy(id = "input2")
    private WebElement input2;

    @FindBy(id = "input3")
    private WebElement input3;

    @FindBy(className = "uk-button")
    private WebElement submit;

    @FindBy(id = "result-value")
    private WebElement result;

    @PostConstruct
    private void initWebElements() {
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, 10);
    }

    public void open() {
        driver.get(LcsContainer.getInstance().getUrl());
    }

    public boolean isOpen() {
        wait.until(visibilityOf(submit));
        return input1.isEnabled();
    }

    public void solve(String input1, String input2, String input3) {
        fillInput(this.input1, input1);
        fillInput(this.input2, input2);
        fillInput(this.input3, input3);
        submit.click();
    }

    public String getResult() {
        wait.until(attributeToBeNotEmpty(result, "innerText"));
        return result.getText();
    }

    private void fillInput(WebElement input, String text) {
        input.click();
        input.sendKeys(text);
    }

}
