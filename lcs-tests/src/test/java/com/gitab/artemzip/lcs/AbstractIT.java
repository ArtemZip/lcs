package com.gitab.artemzip.lcs;

import com.gitab.artemzip.lcs.support.LcsContainer;
import com.gitab.artemzip.lcs.support.WebDriverDelegate;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.client.RestTemplate;

@SpringBootTest
public abstract class AbstractIT {
    protected static final LcsContainer CONTAINER;
    protected static final RestTemplate REST_CLIENT;

    static {
        CONTAINER = LcsContainer.getInstance();
        REST_CLIENT = new RestTemplateBuilder().rootUri(CONTAINER.getUrl() + "lcs/solve").build();
    }

    @Autowired
    @RegisterExtension
    protected WebDriverDelegate driver;
}
