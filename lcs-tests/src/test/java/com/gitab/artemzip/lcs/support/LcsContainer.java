package com.gitab.artemzip.lcs.support;

import lombok.extern.slf4j.Slf4j;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.output.Slf4jLogConsumer;
import org.testcontainers.containers.wait.strategy.Wait;

import java.time.Duration;

@Slf4j
public class LcsContainer extends GenericContainer<LcsContainer> {
    private static final String LCS_IMAGE = "registry.gitlab.com/artemzip/lcs/app:latest";
    private static LcsContainer instance;

    private LcsContainer() {
        super(LCS_IMAGE);
        withLogConsumer(new Slf4jLogConsumer(log));
        addFixedExposedPort(8080, 8080);
        waitingFor(Wait.forHttp("/").withStartupTimeout(Duration.ofSeconds(30)));
        start();
    }

    public static LcsContainer getInstance() {
        if(instance == null) {
            instance = new LcsContainer();
        }
        return instance;
    }

    public String getUrl() {
        return "http://lcs.host:8080/";
    }
}
