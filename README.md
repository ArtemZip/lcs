# Longest Common Subsequent

[![pipeline status](https://gitlab.com/ArtemZip/lcs/badges/master/pipeline.svg)](https://gitlab.com/ArtemZip/lcs/-/commits/master)

## Setup environment before you start
- Add `lcs.host 127.0.0.1` to `/etc/hosts`.
<br/>
**Reason** is that the ips of localhosts in ci env and local machine is different. So, it is the easiest way to make it similar on all environments   

## How to build

```shell script
$ mvn clean install
```

## How to run

```shell script
$ mvn -pl lcs-docker
```
And open in browser [result](http://lcs.host:8080/)

## Project Description

It's multi-module project for solving algorithmic problem wrapped in microservice with backend and frontend.

##### Modules:
- lcs-backend - Spring app which provides Rest endpoint for solving LCS problem - for running app locally use ```mvn spring-boot:run```
- lcs-ui - Frontend based on VueJS and connected with backend - local run is ```npm run serve```
- lcs-test - Module with integration tests for testing Rest and UI endpoints. Runs against running app in docker container 
- lcs-docker - Module with `Dockerfile` which in moment of building whole project builds an new image with actual version 