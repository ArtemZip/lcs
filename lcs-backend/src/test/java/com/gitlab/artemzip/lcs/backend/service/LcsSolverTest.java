package com.gitlab.artemzip.lcs.backend.service;


import com.gitlab.artemzip.lcs.backend.model.LcsInputData;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

class LcsSolverTest {
    private final LcsSolver lcsSolver = new LcsSolver();
    private final Random random = new Random();

    @Test
    void simpleDataTest() {
        assertEquals("AA", lcsSolver.solve(new LcsInputData("AAAA", "AAA", "AA")).getResult());
        assertEquals("", lcsSolver.solve(new LcsInputData("AAAA", "AAA", "")).getResult());
        assertEquals("AA", lcsSolver.solve(new LcsInputData("ABBA", "ABA", "AA")).getResult());
        assertEquals("AAA", lcsSolver.solve(new LcsInputData("ABABA", "AAA", "ABBBAA")).getResult());
    }


    @Test
    void processLongDataTest() {
        assertDoesNotThrow(() -> lcsSolver.solve(new LcsInputData(
                generateData(),
                generateData(),
                generateData()
        )).getResult().length());
    }

    String generateData() {
        return random.ints(97, 123)
                     .limit(100)
                     .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                     .toString();
    }
}
