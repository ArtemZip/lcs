package com.gitlab.artemzip.lcs.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LcsApplication {

    public static void main(String[] args) {
        SpringApplication.run(LcsApplication.class, args);
    }
}
