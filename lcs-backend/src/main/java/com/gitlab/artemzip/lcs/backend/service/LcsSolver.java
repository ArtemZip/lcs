package com.gitlab.artemzip.lcs.backend.service;

import com.gitlab.artemzip.lcs.backend.model.LcsInputData;
import com.gitlab.artemzip.lcs.backend.model.LcsOutputData;
import org.springframework.stereotype.Service;

import java.util.AbstractMap.SimpleEntry;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

import static java.lang.Math.max;
import static java.util.Comparator.comparingInt;


@Service
public class LcsSolver {

    public LcsOutputData solve(LcsInputData inputData) {
        return new LcsOutputData(inputData, solve(inputData.getInput1(),inputData.getInput2(), inputData.getInput3()));
    }

    //improvement to think: reduce data
    private String solve(String input1, String input2, String input3) {
        int [][][] matrix = new int [input1.length() + 1][input2.length() + 1][input3.length() + 1];
        subsequencesLengthMatrix(input1, input2, input3, matrix);

        return buildLongestSubsequence(input1, input2, input3, matrix);
    }

    /**
     * function for building some longest subsequence by looking through @matrix
     *
     * starting from the end of @param matrix and going through highest values for finding equal chars in each string
     */
    private String buildLongestSubsequence(String input1, String input2, String input3, int[][][] matrix) {
        AtomicInteger i = new AtomicInteger(input1.length());
        AtomicInteger j = new AtomicInteger(input2.length());
        AtomicInteger t = new AtomicInteger(input3.length());
        StringBuilder str = new StringBuilder();

        while (i.get() != 0 && j.get() != 0 && t.get() != 0) {
            if(input1.charAt(i.get()-1) == input2.charAt(j.get()-1) && input2.charAt(j.get()-1) == input3.charAt(t.get()-1)) {
                str.append(input1.charAt(i.get()-1));
                i.decrementAndGet();
                j.decrementAndGet();
                t.decrementAndGet();
            } else {
                Stream.of(
                        createEntry(matrix[i.get() -1][j.get()][t.get()], i::decrementAndGet),
                        createEntry(matrix[i.get()][j.get() -1][t.get()], j::decrementAndGet),
                        createEntry(matrix[i.get()][j.get()][t.get() -1], t::decrementAndGet)
                ).max(comparingInt(SimpleEntry::getKey)).get().getValue().run();
            }
        }
        return str.reverse().toString();
    }

    /**
     *  filling @param matrix with lengths of each subsequence
     */
    private void subsequencesLengthMatrix(String input1, String input2, String input3, int [][][] matrix) {
        for(int i = 1; i <= input1.length(); i++) {
            for(int j = 1; j <= input2.length(); j++) {
                for(int t = 1; t <= input3.length(); t++) {
                    if(input1.charAt(i-1) == input2.charAt(j-1) && input2.charAt(j-1) == input3.charAt(t-1)) {
                        matrix[i][j][t] = matrix[i-1][j-1][t-1] + 1;
                    } else {
                        matrix[i][j][t] = max(matrix[i-1][j][t], max(matrix[i][j-1][t],matrix[i][j][t-1]));
                    }
                }
            }
        }
    }

    /**
     * helpful function with aim to make shorter line
     */
    private SimpleEntry<Integer, Runnable> createEntry(int num, Runnable fun) {
        return new SimpleEntry<>(num, fun);
    }
}
