package com.gitlab.artemzip.lcs.backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LcsInputData {
    @NotBlank
    private String input1;
    @NotBlank
    private String input2;
    @NotBlank
    private String input3;
}
