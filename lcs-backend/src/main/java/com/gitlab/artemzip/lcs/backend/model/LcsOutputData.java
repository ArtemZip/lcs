package com.gitlab.artemzip.lcs.backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LcsOutputData {

    private LcsInputData input;

    private String result;
}
