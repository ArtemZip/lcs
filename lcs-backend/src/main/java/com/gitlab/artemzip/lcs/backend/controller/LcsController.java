package com.gitlab.artemzip.lcs.backend.controller;

import com.gitlab.artemzip.lcs.backend.model.LcsInputData;
import com.gitlab.artemzip.lcs.backend.model.LcsOutputData;
import com.gitlab.artemzip.lcs.backend.service.LcsSolver;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@CrossOrigin("http://lcs.host:8080")
@RequiredArgsConstructor
public class LcsController {
    private final LcsSolver lcsSolver;

    @PostMapping("/lcs/solve")
    public ResponseEntity<LcsOutputData> solve(@RequestBody @Valid LcsInputData input, BindingResult valid) {
        if(valid.getAllErrors().isEmpty()) {
            return ResponseEntity.ok(lcsSolver.solve(input));
        } else {
            return ResponseEntity.unprocessableEntity().build();
        }
    }
}
